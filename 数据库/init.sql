drop database shoppingsystem;
create database `shoppingsystem`;
use shoppingsystem;
create table category
(
    id     bigint auto_increment
        primary key,
    name   varchar(25)  null,
    icon   varchar(200) null,
    pid    bigint       null,
    pname  varchar(25)  null,
    ppid   bigint       null,
    ppname varchar(25)  null
);

create table goodlist
(
    id      bigint auto_increment
        primary key,
    name    varchar(300) null,
    number  int          null,
    price   double       null,
    logo    varchar(200) null,
    addtime datetime     null,
    updtime datetime     null,
    pid     bigint       null
);

create table goodpic
(
    picid  bigint auto_increment
        primary key,
    goodid bigint       null,
    picurl varchar(300) null
);

create table orders
(
    id         int auto_increment
        primary key,
    buyerId    int        not null,
    sellerId   int        not null,
    createTime datetime   null,
    finishTime datetime   null,
    flowNo     mediumtext null,
    realAmount double     null,
    itemList   longtext   null
);

create table shoppingcart
(
    id          int auto_increment
        primary key,
    userId      int      not null,
    goodsItems  longtext null,
    totalAmount double   null,
    realAmount  double   null,
    constraint userId
        unique (userId)
);

create table user
(
    id       int auto_increment
        primary key,
    account  varchar(20)            null,
    password varchar(20)            not null,
    username varchar(20) default '' null,
    addr     longtext               null,
    role     varchar(255)           not null
);

create table account
(
    balance double      null,
    userId  int         null,
    cardNum varchar(20) null,
    constraint account_user__fk
        foreign key (userId) references user (id)
);


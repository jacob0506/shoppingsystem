package com.main.shoppingsystem;

import org.mybatis.generator.api.ShellRunner;

/**
 * 功能描述:自动生成mapper.xml、dao、entity
 * @author: hfanss
 * @date: 2019年6月17日 下午8:25:55
 */
public class GetAutoEntityAndDao {

    // 该配置文件放在src\\main\\resources\\该路径下即可
    public static void main(String[] args) {
        args = new String[] { "-configfile", "src\\main\\resources\\generator.xml", "-overwrite" };
        ShellRunner.main(args);
    }

}


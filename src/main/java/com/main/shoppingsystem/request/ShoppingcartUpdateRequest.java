package com.main.shoppingsystem.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.main.shoppingsystem.entities.OrderItem;

import java.util.List;

public class ShoppingcartUpdateRequest {

    private Integer ShoppingcartId;

    private Integer totalAmount;

    private Integer realAmount;

    private List<OrderItem> itemList;

    public Integer getShoppingcartId() {
        return ShoppingcartId;
    }

    public void setShoppingcartId(Integer orderId) {
        ShoppingcartId = orderId;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getRealAmount() {
        return realAmount;
    }

    public void setRealAmount(Integer realAmount) {
        this.realAmount = realAmount;
    }

    public List<OrderItem> getItemList() {
        return itemList;
    }

    public void setItemList(List<OrderItem> itemList) {
        this.itemList = itemList;
    }
}

package com.main.shoppingsystem.dao;

import com.main.shoppingsystem.entities.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserMapper {
    int deleteByPrimaryKey(int id);

    int insert(User record);

    User selectByPrimaryKey(int id);

    List<User> selectAll();

    int updateByPrimaryKey(User record);

    User selectByUsername(String username);
}
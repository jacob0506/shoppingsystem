package com.main.shoppingsystem.dao;

import com.main.shoppingsystem.entities.Goodlist;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface GoodlistMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Goodlist record);

    Goodlist selectByPrimaryKey(Long id);

    List<Goodlist> selectAll();

    List<Goodlist> selectByPageNo(int pageNo);

    int updateByPrimaryKey(Goodlist record);

    List<Goodlist> selectByCategory(String name,int pageNo);

    Integer selectCountByCategory(String name);
    List<Goodlist> selectByCatAndName(String category,String goodsName,int pageNo);

    Integer selectCountByCatAndName(String category,String goodsName);

    List<Goodlist> findByName(String goodsName,int pageNo);

    Integer findCountByName(String goodsName);
}
package com.main.shoppingsystem.dao;

import com.main.shoppingsystem.entities.Orders;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface OrdersMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Orders record);

    Orders selectByPrimaryKey(Integer id);

    List<Orders> selectByUserId(Integer userId);

    List<Orders> selectAll();

    int updateByPrimaryKey(Orders record);
}
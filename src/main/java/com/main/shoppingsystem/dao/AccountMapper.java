package com.main.shoppingsystem.dao;

import com.main.shoppingsystem.entities.Account;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AccountMapper {
    int insert(Account record);

    List<Account> selectAll();
}
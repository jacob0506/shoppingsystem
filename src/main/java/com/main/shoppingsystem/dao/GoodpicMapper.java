package com.main.shoppingsystem.dao;

import com.main.shoppingsystem.entities.Goodpic;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface GoodpicMapper {
    int deleteByPrimaryKey(Long picid);

    int insert(Goodpic record);

    int createByUrl(Long goodsId,String goodsPic);

    Goodpic selectByPrimaryKey(Long picid);

    List<Goodpic> selectByGoodsId(Long goodsId);

    List<Goodpic> selectAll();

    int updateByPrimaryKey(Goodpic record);
}
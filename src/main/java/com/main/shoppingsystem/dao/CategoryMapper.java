package com.main.shoppingsystem.dao;

import com.main.shoppingsystem.entities.Category;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CategoryMapper {
    int deleteByPrimaryKey(int id);

    int insert(Category record);

    Category selectByPrimaryKey(int id);

    List<Category> selectByCatName(String category);

    List<Category> selectByLength(int length);

    List<Category> selectAll();

    int updateByPrimaryKey(Category record);
}
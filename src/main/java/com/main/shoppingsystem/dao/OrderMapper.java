package com.main.shoppingsystem.dao;

import com.main.shoppingsystem.entities.Order;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface OrderMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Order record);

    Order selectByPrimaryKey(Integer id);

    Order selectByUserId(Integer userId);

    List<Order> selectAll();

    int updateByPrimaryKey(Order record);
}
package com.main.shoppingsystem.dao;

import com.main.shoppingsystem.entities.Shoppingcart;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ShoppingcartMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Shoppingcart record);

    Shoppingcart selectByPrimaryKey(Integer id);

    Shoppingcart selectByUserId(Integer userId);

    List<Shoppingcart> selectAll();

    int updateByPrimaryKey(Shoppingcart record);
}
package com.main.shoppingsystem.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.main.shoppingsystem.dao.GoodpicMapper;
import com.main.shoppingsystem.dao.UserMapper;
import com.main.shoppingsystem.entities.Goodpic;
import com.main.shoppingsystem.entities.GoodsItem;
import com.main.shoppingsystem.entities.Orders;
import com.main.shoppingsystem.entities.OrderItem;
import com.main.shoppingsystem.entities.model.OrderCreateModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TransfromUtil {

    @Autowired
    private GoodpicMapper goodpicMapper;

    @Autowired
    private UserMapper userMapper;

    public  List<GoodsItem> transformOrdToGo(List<OrderItem> orderItems){
        List<GoodsItem> goodsItems = new ArrayList<>();
        for(OrderItem item : orderItems){
            GoodsItem temp = new GoodsItem();
            temp.setAmount((double)item.getAmount());
            temp.setGoodsName(item.getGoodsName());
            System.out.println(item.getGoodsId());
            List<Goodpic> goodpics = goodpicMapper.selectByGoodsId(item.getGoodsId());
            if(!goodpics.isEmpty()) {
                temp.setGoodsPicurl(goodpicMapper.selectByGoodsId(item.getGoodsId()).get(0).getPicurl());
            }
            temp.setItemCount(item.getItemCount());
            temp.setGoodsId(item.getGoodsId());
            goodsItems.add(temp);
        }
        return goodsItems;
    }

    public OrderCreateModel transformOrdToOCM(Orders orders){
        OrderCreateModel temp = new OrderCreateModel();
        temp.setBuyer(userMapper.selectByPrimaryKey(orders.getBuyerid()).getUsername());
        temp.setSeller("C10 213");
        temp.setCreateTime(new Date());
        temp.setRealAmount(orders.getRealAmount());
        temp.setFlowNo(orders.getFlowNo());
        temp.setItemList(JSON.parseObject(orders.getItemInfoList(),new TypeReference<List<OrderItem>>(){}));
        temp.setSendAddr("C10 213");
        if(userMapper.selectByPrimaryKey(orders.getBuyerid()).getAddrItemList()!=null){
            temp.setReceivedAddr(userMapper.selectByPrimaryKey(orders.getBuyerid()).getAddrItemList().get(1).getAddr());
        }
        temp.setTotalAmount(orders.getRealAmount());
        return temp;
    }
}

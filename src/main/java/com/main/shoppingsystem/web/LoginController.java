package com.main.shoppingsystem.web;

import cn.hutool.core.lang.Assert;
import cn.hutool.crypto.SecureUtil;
import com.main.shoppingsystem.entities.ResultJSON;
import com.main.shoppingsystem.entities.Role;
import com.main.shoppingsystem.entities.User;
import com.main.shoppingsystem.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@CrossOrigin
public class LoginController {

    @Autowired
    private UserService userService;

    @RequestMapping("/register")
    public int register(@RequestParam("username") String username, @RequestParam("password") String password)
    {

        return userService.register(username, password,Role.USER);
    }


}

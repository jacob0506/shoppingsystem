package com.main.shoppingsystem.web;

import com.main.shoppingsystem.entities.Category;
import com.main.shoppingsystem.entities.Goodlist;
import com.main.shoppingsystem.entities.ResultJSON;
import com.main.shoppingsystem.entities.model.GoodsQueryModel;
import com.main.shoppingsystem.entities.model.IndexViewModel;
import com.main.shoppingsystem.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/index")
public class IndexController {

    @Autowired
    private GoodsService goodsService;

    @GetMapping("/queryGoods")
    public ResultJSON queryGoods(@RequestParam("pageNo") Integer pageNo){
        try{
            List<Goodlist> goodlists = goodsService.findAllByPageNo(pageNo);
            Integer maxPageNo = (goodsService.selectAll().size()+9) / 10;
            GoodsQueryModel model = new GoodsQueryModel();
            model.setGoodlists(goodlists);
            model.setMaxPageNo(maxPageNo);
            return new ResultJSON(200,"查询成功",model);
        }
        catch (Exception e){
            e.printStackTrace();
            return new ResultJSON(-1,"查询失败",null);
        }
    }

    @GetMapping("/queryGoodsByCat")
    public ResultJSON queryGoodsByCat(@RequestParam("name") String category,@RequestParam("pageNo") Integer pageNo){
        try{
            if(category.length()<1){return new ResultJSON(-1,"分类名不能为空",null);}
            List<Goodlist> goodlists = goodsService.selectByCategory(category,pageNo);
            Integer maxPageNo = (goodsService.selectCountByCategory(category) + 9) / 10;
            GoodsQueryModel model = new GoodsQueryModel();
            model.setGoodlists(goodlists);
            model.setMaxPageNo(maxPageNo);
            return new ResultJSON(200,"查询成功",model);
        }
        catch (Exception e){
            e.printStackTrace();
            return new ResultJSON(-1,"查询失败",null);
        }
    }

    @GetMapping("/queryGoodsByName")
    public ResultJSON queryGoodsByName(@RequestParam("goodsName") String goodsName,@RequestParam("pageNo") Integer pageNo){
        try{
            if(goodsName.length()<1){
                List<Goodlist> goodlists = goodsService.findAllByPageNo(pageNo);
                Integer maxPageNo = (goodsService.selectAll().size() + 9) / 10;
                if(pageNo > maxPageNo){
                    return new ResultJSON(-1,"错误，已超出最大页数",null);
                }
                GoodsQueryModel model = new GoodsQueryModel();
                model.setGoodlists(goodlists);
                model.setMaxPageNo(maxPageNo);
                return new ResultJSON(200,"查询成功",model);
            }
            List<Goodlist> goodlists = goodsService.findByName("'"+goodsName+"'",pageNo);
            Integer maxPageNo = (goodsService.findCountByName("'"+goodsName+"'") + 9) / 10;
            GoodsQueryModel model = new GoodsQueryModel();
            model.setGoodlists(goodlists);
            model.setMaxPageNo(maxPageNo);
            //System.out.println(goodlists.get(0).getName());
            return new ResultJSON(200,"查询成功",model);
        }
        catch (Exception e){
            e.printStackTrace();
            return new ResultJSON(-1,"查询失败",null);
        }
    }

    @GetMapping("/queryGoodsByCatAndName")
    public ResultJSON queryGoodsByCatAndName(@RequestParam("category") String category,
                                             @RequestParam("goodsName") String goodsName,
                                             @RequestParam("pageNo") Integer pageNo){
        try{
            if(category.length()<1&&goodsName.length()>1){
                GoodsQueryModel model = new GoodsQueryModel();
                Integer maxPageNo = (goodsService.findCountByName("'"+goodsName+"'") + 9) / 10;
                if(pageNo > maxPageNo){
                    return new ResultJSON(-1,"错误，已超出最大页数",null);
                }
                model.setGoodlists(goodsService.findByName("'"+goodsName+"'",pageNo));
                model.setMaxPageNo(maxPageNo);
                return new ResultJSON(200,"查询成功",model);
            } else if (category.length()>1&&goodsName.length()<1) {
                GoodsQueryModel model = new GoodsQueryModel();
                Integer maxPageNo = (goodsService.selectCountByCategory(category)+ 9) / 10;
                if(pageNo > maxPageNo){
                    return new ResultJSON(-1,"错误，已超出最大页数",null);
                }
                model.setGoodlists(goodsService.selectByCategory(category,pageNo));
                model.setMaxPageNo(maxPageNo);
                return new ResultJSON(200,"查询成功",model);
            } else if (category.length()>1&&goodsName.length()>1) {
                Integer maxPageNo = (goodsService.selectCountByCatAndName(category,"'"+goodsName+"'") + 9) / 10;
                if(pageNo > maxPageNo){
                    return new ResultJSON(-1,"错误，已超出最大页数",null);
                }
                GoodsQueryModel model = new GoodsQueryModel();
                model.setMaxPageNo(maxPageNo);
                model.setGoodlists(goodsService.selectByCatAndName(category,"'"+goodsName+"'",pageNo));
                return new ResultJSON(200,"查询成功",model);
            }
            else {
                return new ResultJSON(200,"无符合商品",0);
            }
        }
        catch (Exception e){
            e.printStackTrace();
            return new ResultJSON(-1,"查询失败",null);
        }
    }

    @GetMapping("/indexView")
    public ResultJSON view(){
        try{
            List<Goodlist> goodlists = goodsService.findAllByPageNo(0);
            List<Category> categories = goodsService.findCategory(15);
            //System.out.println(goodlists.get(0).getName());
            IndexViewModel indexViewModel = new IndexViewModel();
            indexViewModel.setCategories(categories);
            indexViewModel.setGoodlists(goodlists);
            return new ResultJSON(200,"查询成功",indexViewModel);
        }
        catch (Exception e){
            e.printStackTrace();
            return new ResultJSON(-1,"访问失败",null);
        }
    }

    @GetMapping("/queryAllCat")
    public ResultJSON queryAllCat(){
        try{
            List<Category> categories = goodsService.findAllCat();
            return new ResultJSON(200,"查询成功",categories);
        }
        catch (Exception e){
            e.printStackTrace();
            return new ResultJSON(-1,"访问失败",null);
        }
    }



}

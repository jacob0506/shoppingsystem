package com.main.shoppingsystem.web;

import com.main.shoppingsystem.entities.*;
import com.main.shoppingsystem.entities.model.CategoryCreateModel;
import com.main.shoppingsystem.entities.model.GoodsCreateModel;
import com.main.shoppingsystem.service.GoodsService;
import com.main.shoppingsystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.logging.Handler;

@RestController
@RequestMapping("/admin")
public class admin {

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private UserService userService;


    @GetMapping()
    public ResultJSON checkAmdin(HttpServletRequest request){
        User user = (User)request.getSession().getAttribute("ADMIN");
        if(user.getRole()!=Role.ADMIN){
            return new ResultJSON(-1, "请登录管理员", null);
        }
        else return new ResultJSON(200, "欢迎", 1);
    }

    @PostMapping("/adminLogin")
    public ResultJSON adminLogin(@RequestParam("username") String username, @RequestParam("password") String password, HttpSession session) {
        try {
            User user = userService.selectByName(username);
            if(user==null){
                return new ResultJSON(-1, "没有该用户", null);
            }
            else if(!user.getPassword().equals(password)){
                return new ResultJSON(-1, "密码错误", null);
            }
            else {
                session.setAttribute("ADMIN",user);
                return new ResultJSON(200, "登录成功", user);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultJSON(-1, "登录失败", null);
        }
    }

    //@PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/addGoods")
    public ResultJSON addGoods(@RequestBody GoodsCreateModel goods, HttpServletRequest request) {
        try {
            User user = (User)request.getSession().getAttribute("ADMIN");
            if(user.getRole()== Role.ADMIN) {
                Goodlist goodlist = new Goodlist();
                goodlist.setName(goods.getName());
                goodlist.setNumber(goods.getNumber());
                goodlist.setLogo(goods.getLogo());
                goodlist.setPrice(goods.getPrice());
                goodlist.setAddtime(new Date());
                goodlist.setPid(goodsService.selectByCatName(goods.getCategory()).get(0).getId());
                goodsService.createGoods(goodlist);
                Long goodsId = goodlist.getId();
                System.out.println(goods.getPiclist().size());
                System.out.println(goods.getPiclist());
                for (String picurl : goods.getPiclist()) {
                    goodsService.createGoodsPicByUrl(goodsId, picurl);
                }
                int res = 1;
                return new ResultJSON(200, "上架商品成功", res);
            }
            else return new ResultJSON(-1, "请登录管理员", null);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultJSON(-1, "上架失败", null);
        }
    }

    //@PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/addCategory")
    public ResultJSON addCategory(@RequestBody CategoryCreateModel createModel, HttpServletRequest request) {
        try {
            User user = (User)request.getSession().getAttribute("ADMIN");
            if(user.getRole()== Role.ADMIN) {
                Category category = new Category();
                category.setIcon(createModel.getIcon());
                category.setName(createModel.getName());
                int res = goodsService.createCat(category);
                return new ResultJSON(200, "添加分类成功", res);
            }
            else return new ResultJSON(-1, "请登录管理员", null);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultJSON(-1, "添加失败", null);
        }
    }
}

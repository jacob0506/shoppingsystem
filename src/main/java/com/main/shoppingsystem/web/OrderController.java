package com.main.shoppingsystem.web;

import com.main.shoppingsystem.entities.Role;
import com.main.shoppingsystem.entities.SystemUser;
import com.main.shoppingsystem.entities.model.OrderCreateModel;
import com.main.shoppingsystem.entities.ResultJSON;
import com.main.shoppingsystem.request.OrderCreateRequest;
import com.main.shoppingsystem.request.OrderUpdateRequest;
import com.main.shoppingsystem.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@RestController
@RequestMapping("/orderBase")
public class OrderController {

    @Autowired
    private OrderService orderService;

    //@PreAuthorize("hasAnyRole('USER','AMDIN')")
    @PostMapping("/createOrderBase")
    public ResultJSON createOrderBase(@RequestBody OrderCreateRequest request, HttpServletRequest httpServletRequest) {
        try {
            SystemUser systemUser = (SystemUser)httpServletRequest.getSession().getAttribute("user");
            if(systemUser.getRole()== Role.USER||systemUser.getRole()== Role.ADMIN) {
                OrderCreateModel orderCreateModel = orderService.createOrder(request);
                return new ResultJSON(200, "创建订单成功", orderCreateModel);
            }
            else return new ResultJSON(-1, "请登录", null);
        }
        catch (Exception e){
            e.printStackTrace();
            return new ResultJSON(-1,"创建订单失败",null);
        }
    }

    //@PreAuthorize("hasAnyRole('USER','AMDIN')")
    @PostMapping("/updateOrderBase")
    public ResultJSON updateOrderBase(@RequestBody OrderUpdateRequest request,HttpServletRequest httpServletRequest) {
        try {
            SystemUser systemUser = (SystemUser)httpServletRequest.getSession().getAttribute("user");
            if(systemUser.getRole()== Role.USER||systemUser.getRole()== Role.ADMIN) {
                boolean res = orderService.updateOrder(request);
                return new ResultJSON(200, "修改订单成功", res);
            }
            else return new ResultJSON(-1, "请登录", null);
        }
        catch (Exception e){
            e.printStackTrace();
            return new ResultJSON(-1,"修改订单失败",null);
        }
    }

    //@PreAuthorize("hasAnyRole('USER','AMDIN')")
    @GetMapping("/queryOrderBaseByUserId")
    public ResultJSON queryOrderBase(@RequestParam("userId") Integer userId, HttpServletRequest httpServletRequest) {
        try {
            SystemUser systemUser = (SystemUser)httpServletRequest.getSession().getAttribute("user");
            if(systemUser.getRole()== Role.USER||systemUser.getRole()== Role.ADMIN) {
                List<OrderCreateModel> models = orderService.findByUserId(userId);
                return new ResultJSON(200, "查询订单成功", models);
            }
            else return new ResultJSON(-1, "请登录", null);
        }
        catch (Exception e){
            e.printStackTrace();
            return new ResultJSON(-1,"查询订单失败",null);
        }
    }
}

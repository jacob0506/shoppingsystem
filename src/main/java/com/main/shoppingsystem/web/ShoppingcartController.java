package com.main.shoppingsystem.web;

import com.main.shoppingsystem.entities.OrderItem;
import com.main.shoppingsystem.entities.ResultJSON;
import com.main.shoppingsystem.entities.Role;
import com.main.shoppingsystem.entities.SystemUser;
import com.main.shoppingsystem.entities.model.ShoppingcartModel;
import com.main.shoppingsystem.request.ShoppingcartUpdateRequest;
import com.main.shoppingsystem.service.ShoppingcartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/shoppingcart")
public class ShoppingcartController {

    @Autowired
    private ShoppingcartService shoppingcartService;

    //@PreAuthorize("hasAnyRole('USER','AMDIN')")
    @PostMapping("/addGoodsItem")
    public ResultJSON addGoodsItem(@RequestBody OrderItem goods,
                                   HttpServletRequest request) {
        try {
            SystemUser systemUser = (SystemUser)request.getSession().getAttribute("user");
            //System.out.println(systemUser.getUsername());
            if(systemUser.getRole()== Role.USER||systemUser.getRole()== Role.ADMIN){
                if(goods==null){System.out.println("null");}
                System.out.println(goods.getGoodsName()+" "+goods.getAmount().toString());
                int res = shoppingcartService.addToShoppingcart(goods, systemUser.getId());
                return new ResultJSON(200, "添加到购物车成功", res);
            }
            else return new ResultJSON(-1, "请登录", null);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultJSON(-1, "添加失败", null);
        }
    }

    //@PreAuthorize("hasAnyRole('USER','AMDIN')")
    @GetMapping("/queryByUserId")
    public ResultJSON findByUserId(@RequestParam("userId") Integer userId,HttpServletRequest request) {
        try {
            SystemUser systemUser = (SystemUser)request.getSession().getAttribute("user");
            if(systemUser.getRole()== Role.USER||systemUser.getRole()== Role.ADMIN){
            ShoppingcartModel shoppingcartModel = shoppingcartService.findByUserId(userId);
            return new ResultJSON(200, "查询购物车成功", shoppingcartModel);
            }
            else return new ResultJSON(-1, "请登录", null);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultJSON(-1, "添加失败", null);
        }
    }

    //@PreAuthorize("hasAnyRole('USER','AMDIN')")
    @PostMapping("/updateShoppingcart")
    public ResultJSON update(@RequestBody ShoppingcartUpdateRequest shoppingcartUpdateRequest,HttpServletRequest request) {
        try {
            SystemUser systemUser = (SystemUser)request.getSession().getAttribute("user");
            if(systemUser.getRole()== Role.USER||systemUser.getRole()== Role.ADMIN) {
                int res = shoppingcartService.updateShoppingcart(shoppingcartUpdateRequest);
                return new ResultJSON(200, "更新购物车成功", res);
            }
            else return new ResultJSON(-1, "请登录", null);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultJSON(-1, "添加失败", null);
        }
    }
}

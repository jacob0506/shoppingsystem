package com.main.shoppingsystem.web;

import com.main.shoppingsystem.entities.Goodlist;
import com.main.shoppingsystem.entities.Goodpic;
import com.main.shoppingsystem.entities.ResultJSON;
import com.main.shoppingsystem.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/goods")
public class GoodsController {
    @Autowired
    private GoodsService goodsService;

    @GetMapping("/getPic")
    public ResultJSON getPic(@RequestParam("goodsId") Long goodsId){
        try{
            List<Goodpic> goodsPicList = goodsService.findPicAll(goodsId);
            return new ResultJSON(200,"查询成功",goodsPicList);
        }
        catch (Exception e){
            e.printStackTrace();
            return new ResultJSON(-1,"查询失败",null);
        }
    }
}

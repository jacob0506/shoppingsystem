package com.main.shoppingsystem.entities;

public class AddrItem {
    private String addr;

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }
}

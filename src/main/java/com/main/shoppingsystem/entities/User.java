package com.main.shoppingsystem.entities;

import java.util.Date;
import java.util.List;

public class User {
    private int id;

    private String username;

    private String password;

    private String account;

    private Role role;

    private List<AddrItem> addrItemList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public List<AddrItem> getAddrItemList() {
        return addrItemList;
    }

    public void setAddrItemList(List<AddrItem> addrItemList) {
        this.addrItemList = addrItemList;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
package com.main.shoppingsystem.entities;

public class Shoppingcart {
    private Integer id;

    private Integer userid;

    private Double totalamount;

    private Double realamount;

    private String goodsitems;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Double getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(Double totalamount) {
        this.totalamount = totalamount;
    }

    public Double getRealamount() {
        return realamount;
    }

    public void setRealamount(Double realamount) {
        this.realamount = realamount;
    }

    public String getGoodsitems() {
        return goodsitems;
    }

    public void setGoodsitems(String goodsitems) {
        this.goodsitems = goodsitems == null ? null : goodsitems.trim();
    }
}
package com.main.shoppingsystem.entities;

public enum Role {
    USER,
    ADMIN
}

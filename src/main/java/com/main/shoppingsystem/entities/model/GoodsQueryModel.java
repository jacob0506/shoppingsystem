package com.main.shoppingsystem.entities.model;

import com.main.shoppingsystem.entities.Goodlist;

import java.util.List;

public class GoodsQueryModel {
    private List<Goodlist> goodlists;

    private Integer maxPageNo;

    public List<Goodlist> getGoodlists() {
        return goodlists;
    }

    public void setGoodlists(List<Goodlist> goodlists) {
        this.goodlists = goodlists;
    }

    public Integer getMaxPageNo() {
        return maxPageNo;
    }

    public void setMaxPageNo(Integer maxPageNo) {
        this.maxPageNo = maxPageNo;
    }
}

package com.main.shoppingsystem.entities.model;

import java.util.List;

public class CategoryCreateModel {
    private String name;

    private String icon;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}

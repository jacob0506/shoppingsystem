package com.main.shoppingsystem.entities.model;

import com.main.shoppingsystem.entities.GoodsItem;
import com.main.shoppingsystem.entities.OrderItem;

import java.util.List;

public class ShoppingcartModel {

    private Integer ShoppingcartId;

    private Double totalAmount;

    private Double realAmount;

    private List<GoodsItem> itemList;

    public Integer getShoppingcartId() {
        return ShoppingcartId;
    }

    public void setShoppingcartId(Integer orderId) {
        ShoppingcartId = orderId;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getRealAmount() {
        return realAmount;
    }

    public void setRealAmount(Double realAmount) {
        this.realAmount = realAmount;
    }

    public List<GoodsItem> getItemList() {
        return itemList;
    }

    public void setItemList(List<GoodsItem> itemList) {
        this.itemList = itemList;
    }
}

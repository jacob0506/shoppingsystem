package com.main.shoppingsystem.entities.model;

import com.main.shoppingsystem.entities.Category;
import com.main.shoppingsystem.entities.Goodlist;

import java.util.List;

public class IndexViewModel {
    private List<Category> categories;

    private List<Goodlist> goodlists;

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<Goodlist> getGoodlists() {
        return goodlists;
    }

    public void setGoodlists(List<Goodlist> goodlists) {
        this.goodlists = goodlists;
    }
}

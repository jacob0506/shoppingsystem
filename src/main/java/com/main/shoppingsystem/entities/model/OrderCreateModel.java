package com.main.shoppingsystem.entities.model;

import com.main.shoppingsystem.entities.OrderItem;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class OrderCreateModel {

    private Long flowNo;

    private Double totalAmount;

    private Double realAmount;

    private String buyer;

    private String seller;

    private List<OrderItem> itemList;

    private String sendAddr;

    private String receivedAddr;

    private Date createTime;

    public Long getFlowNo() {
        return flowNo;
    }

    public void setFlowNo(Long flowNo) {
        this.flowNo = flowNo;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getRealAmount() {
        return realAmount;
    }

    public void setRealAmount(Double realAmount) {
        this.realAmount = realAmount;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public List<OrderItem> getItemList() {
        return itemList;
    }

    public void setItemList(List<OrderItem> itemList) {
        this.itemList = itemList;
    }

    public String getSendAddr() {
        return sendAddr;
    }

    public void setSendAddr(String sendAddr) {
        this.sendAddr = sendAddr;
    }

    public String getReceivedAddr() {
        return receivedAddr;
    }

    public void setReceivedAddr(String receivedAddr) {
        this.receivedAddr = receivedAddr;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}

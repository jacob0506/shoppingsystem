package com.main.shoppingsystem.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class OrderItem implements Serializable {

    @JsonProperty("Amount")
    private Integer Amount;

    @JsonProperty("itemCount")
    private Integer itemCount;

    @JsonProperty("goodsName")
    private String goodsName;

    @JsonProperty("goodsId")
    private Long goodsId;

    public Integer getAmount() {
        return Amount;
    }

    public void setAmount(Integer amount) {
        Amount = amount;
    }

    public Integer getItemCount() {
        return itemCount;
    }

    public void setItemCount(Integer itemCount) {
        this.itemCount = itemCount;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public OrderItem(){

    }
}

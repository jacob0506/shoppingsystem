package com.main.shoppingsystem.entities;

import org.springframework.security.core.parameters.P;

import java.util.Date;
import java.util.List;

public class Order {
    private Integer id;

    private Integer buyerid;

    private Integer sellerid;

    private Date createtime;

    private Date finishtime;

    private Long flowNo;

    private Double realAmount;

    private String itemInfoList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBuyerid() {
        return buyerid;
    }

    public void setBuyerid(Integer buyerid) {
        this.buyerid = buyerid;
    }

    public Integer getSellerid() {
        return sellerid;
    }

    public void setSellerid(Integer sellerid) {
        this.sellerid = sellerid;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getFinishtime() {
        return finishtime;
    }

    public void setFinishtime(Date finishtime) {
        this.finishtime = finishtime;
    }

    public String getItemInfoList() {
        return itemInfoList;
    }

    public void setItemInfoList(String itemInfoList) {
        this.itemInfoList = itemInfoList;
    }

    public Long getFlowNo() {
        return flowNo;
    }

    public void setFlowNo(Long flowNo) {
        this.flowNo = flowNo;
    }

    public Double getRealAmount() {
        return realAmount;
    }

    public void setRealAmount(Double realAmount) {
        this.realAmount = realAmount;
    }
}
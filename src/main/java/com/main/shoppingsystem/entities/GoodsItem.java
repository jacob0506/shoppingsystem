package com.main.shoppingsystem.entities;

public class GoodsItem {

    private Double Amount;

    private Integer itemCount;

    private String goodsName;

    private Long goodsId;

    private String goodsPicurl;

    public String getGoodsPicurl() {
        return goodsPicurl;
    }

    public void setGoodsPicurl(String goodsPicurl) {
        this.goodsPicurl = goodsPicurl;
    }

    public Double getAmount() {
        return Amount;
    }

    public void setAmount(Double amount) {
        Amount = amount;
    }

    public Integer getItemCount() {
        return itemCount;
    }

    public void setItemCount(Integer itemCount) {
        this.itemCount = itemCount;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }
}

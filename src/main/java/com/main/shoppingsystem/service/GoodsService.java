package com.main.shoppingsystem.service;

import com.main.shoppingsystem.dao.CategoryMapper;
import com.main.shoppingsystem.dao.GoodlistMapper;
import com.main.shoppingsystem.dao.GoodpicMapper;
import com.main.shoppingsystem.entities.Category;
import com.main.shoppingsystem.entities.Goodlist;
import com.main.shoppingsystem.entities.Goodpic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsService {

    @Autowired
    private GoodlistMapper goodlistMapper;

    @Autowired
    private GoodpicMapper goodpicMapper;

    @Autowired
    private CategoryMapper categoryMapper;

    public int createGoods(Goodlist goodlist){
        return goodlistMapper.insert(goodlist);
    }

    public int updateGoods(Goodlist goodlist) {return goodlistMapper.updateByPrimaryKey(goodlist);}

    public Goodlist findById(Long id) {return goodlistMapper.selectByPrimaryKey(id);}

    public List<Goodlist> selectByCategory(String name,int pageNo) {return goodlistMapper.selectByCategory(name, pageNo);}

    public Integer selectCountByCategory(String name) {return goodlistMapper.selectCountByCategory(name);}
    public List<Goodlist> selectByCatAndName(String category,String goodsName,int pageNo) {
        return goodlistMapper.selectByCatAndName(category,goodsName,pageNo);
    }

    public Integer selectCountByCatAndName(String category,String goodsName) {
        return goodlistMapper.selectCountByCatAndName(category,goodsName);
    }
    public List<Goodlist> findByName(String goodsName,int pageNo) {return goodlistMapper.findByName(goodsName, pageNo);}

    public Integer findCountByName(String goodsName) {return goodlistMapper.findCountByName(goodsName);}

    public List<Goodlist> findAllByPageNo(int pageNo) {return goodlistMapper.selectByPageNo(pageNo);}

    public List<Goodlist> selectAll() {return goodlistMapper.selectAll();}

    public List<Category> findCategory(int length) {return categoryMapper.selectByLength(length);}

    public List<Category> findAllCat() {return categoryMapper.selectAll();}

    public List<Goodpic> findPicAll(Long goodsId) {return goodpicMapper.selectByGoodsId(goodsId);}

    public int createGoodsPic(Goodpic goodpic) {return goodpicMapper.insert(goodpic);}

    public int createCat(Category category) {return categoryMapper.insert(category);}

    public int createGoodsPicByUrl(Long goodsId,String goodsPic) {
        Goodpic goodpic = new Goodpic();
        goodpic.setGoodid(goodsId);
        goodpic.setPicurl(goodsPic);
        return goodpicMapper.insert(goodpic);
    }

    public List<Category> selectByCatName(String category) {return categoryMapper.selectByCatName(category);}
}

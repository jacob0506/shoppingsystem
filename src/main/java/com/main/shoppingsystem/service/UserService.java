package com.main.shoppingsystem.service;

import com.main.shoppingsystem.dao.ShoppingcartMapper;
import com.main.shoppingsystem.dao.UserMapper;
import com.main.shoppingsystem.entities.Role;
import com.main.shoppingsystem.entities.Shoppingcart;
import com.main.shoppingsystem.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ShoppingcartMapper shoppingcartMapper;

    public User selectById(int id){
        return userMapper.selectByPrimaryKey(id);
    }

    public User selectByName(String username) {return userMapper.selectByUsername(username);}

    public int register(String userName, String password, Role role){
        User User1 = userMapper.selectByUsername(userName);
        if(User1 != null) return -1;
        User temp = new User();
        temp.setUsername(userName);
        temp.setPassword(password);
        temp.setRole(role);
        Shoppingcart shoppingcart = new Shoppingcart();
        int res = userMapper.insert(temp);
        shoppingcart.setUserid(temp.getId());
        shoppingcartMapper.insert(shoppingcart);
        return res;
    }
}

package com.main.shoppingsystem.service;

import com.main.shoppingsystem.dao.UserMapper;
import com.main.shoppingsystem.entities.SystemUser;
import com.main.shoppingsystem.entities.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

//自定义用户认证的核心逻辑
@Service
public class MyUserDetailsService implements UserDetailsService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userMapper.selectByUsername(username);
        if(user == null){
            throw new UsernameNotFoundException("用户不存在");
        }
        List<GrantedAuthority> auths =
                AuthorityUtils.createAuthorityList(user.getRole().toString());
        return new SystemUser(user.getId(),user.getUsername(),passwordEncoder.encode(user.getPassword()),auths,user.getRole());
    }
}

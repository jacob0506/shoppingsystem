package com.main.shoppingsystem.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.main.shoppingsystem.dao.ShoppingcartMapper;
import com.main.shoppingsystem.entities.OrderItem;
import com.main.shoppingsystem.entities.Shoppingcart;
import com.main.shoppingsystem.entities.model.ShoppingcartModel;
import com.main.shoppingsystem.request.ShoppingcartUpdateRequest;
import com.main.shoppingsystem.util.TransfromUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ShoppingcartService {

    @Autowired
    private ShoppingcartMapper shoppingcartMapper;

    @Autowired
    private TransfromUtil transfromUtil;

    public int updateShoppingcart(ShoppingcartUpdateRequest request) {
        System.out.println(request.getShoppingcartId());
        Shoppingcart temp = shoppingcartMapper.selectByPrimaryKey(request.getShoppingcartId());
        if(temp==null){
            System.out.println("temp==null");
            return -1;
        }
        if(request.getItemList()==null){
            temp.setGoodsitems(null);
        }
        else {
            temp.setGoodsitems(JSON.toJSONString(request.getItemList()));
        }
        temp.setRealamount((double)request.getRealAmount());
        temp.setTotalamount((double)request.getTotalAmount());
        return shoppingcartMapper.updateByPrimaryKey(temp);
    }

    public ShoppingcartModel findByUserId(Integer id) {
        Shoppingcart shoppingcart = shoppingcartMapper.selectByUserId(id);
        ShoppingcartModel shoppingcartModel = new ShoppingcartModel();
        shoppingcartModel.setShoppingcartId(shoppingcart.getId());
        if(shoppingcart.getGoodsitems().length()>4){
            List<OrderItem> orderList = JSON.parseObject(shoppingcart.getGoodsitems(),new TypeReference<List<OrderItem>>(){});
            shoppingcartModel.setItemList(transfromUtil.transformOrdToGo(orderList));
        }
        shoppingcartModel.setTotalAmount(shoppingcart.getTotalamount());
        shoppingcartModel.setRealAmount(shoppingcart.getRealamount());
        return shoppingcartModel;
    }
    public int addToShoppingcart(OrderItem goods, Integer userId) {
        Shoppingcart shoppingcart = shoppingcartMapper.selectByUserId(userId);
        List<OrderItem> goodsList = JSON.parseObject(shoppingcart.getGoodsitems(),new TypeReference<List<OrderItem>>(){});
        if(goodsList==null){goodsList = new ArrayList<>();}
        if(!goodsList.isEmpty()){
            for(OrderItem item : goodsList){
                if(item.getGoodsId()==goods.getGoodsId()){
                    item.setItemCount(item.getItemCount()+goods.getItemCount());
                    item.setAmount(item.getAmount()+goods.getAmount());
                    goods=null;
                    break;
                }
            }
        }
        if(goods!=null){
           goodsList.add(goods);
        }
        shoppingcart.setGoodsitems(JSON.toJSONString(goodsList));
        // TODO: 2022/11/25 更新后需重新计算购物车总额和实际金额,目前没实现优惠劵功能,所以TotalAmount和RealAmount是一样的
        Double realAmount = shoppingcart.getRealamount();
        if(realAmount==null){realAmount = 0.0;}
        System.out.println(realAmount);
        System.out.println(goods.getAmount());
        realAmount += goods.getAmount();
        shoppingcart.setRealamount(realAmount);
        shoppingcart.setTotalamount(realAmount);
        return shoppingcartMapper.updateByPrimaryKey(shoppingcart);
        }
}

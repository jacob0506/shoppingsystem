package com.main.shoppingsystem.service;

import com.main.shoppingsystem.dao.OrdersMapper;
import com.main.shoppingsystem.dao.UserMapper;
import com.main.shoppingsystem.entities.Orders;
import com.main.shoppingsystem.entities.model.OrderCreateModel;
import com.main.shoppingsystem.request.OrderCreateRequest;
import com.main.shoppingsystem.request.OrderUpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.alibaba.fastjson.JSON;
import com.main.shoppingsystem.util.TransfromUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    private OrdersMapper ordersMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private  FlowNoFacade flowNoFacade;

    @Autowired
    private TransfromUtil transfromUtil;

    public OrderCreateModel createOrder(OrderCreateRequest request){
        Orders orders = new Orders();
        orders.setBuyerid(request.getBuyerId());
        orders.setSellerid(request.getSellerId());
        orders.setItemInfoList(JSON.toJSONString(request.getItemList()));
        orders.setFlowNo(flowNoFacade.getNextId());
        Date date = new Date();
        orders.setCreatetime(date);
        orders.setRealAmount(request.getRealAmount());
        ordersMapper.insert(orders);
        OrderCreateModel temp = new OrderCreateModel();
        temp.setBuyer(userMapper.selectByPrimaryKey(request.getBuyerId()).getUsername());
        temp.setSeller("C10 213");
        temp.setCreateTime(date);
        temp.setRealAmount(request.getRealAmount());
        temp.setFlowNo(orders.getFlowNo());
        temp.setItemList(request.getItemList());
        temp.setSendAddr("C10 213");
        // TODO: 2022/11/20 收货地址功能未完善
        if(userMapper.selectByPrimaryKey(request.getBuyerId()).getAddrItemList()!=null){
            temp.setReceivedAddr(userMapper.selectByPrimaryKey(request.getBuyerId()).getAddrItemList().get(1).getAddr());
        }
        temp.setTotalAmount(request.getTotalAmount());
        //OrderCreateModel temp = TransfromUtil.transformOrdToOCM(order);
        return temp;
    }

    public boolean updateOrder(OrderUpdateRequest request) {
        Orders orders = ordersMapper.selectByPrimaryKey(request.getOrderId());
        orders.setItemInfoList(JSON.toJSONString(request.getItemList()));
        orders.setRealAmount(request.getRealAmount());
        if(ordersMapper.updateByPrimaryKey(orders)==1){
            return true;
        }
        else {
            return false;
        }
    }

    public List<OrderCreateModel> findByUserId(Integer userId){
        List<Orders> orders = ordersMapper.selectByUserId(userId);
        System.out.println("查询订单"+userId);
        List<OrderCreateModel> orderCreateModels = new ArrayList<>();
        for(Orders order : orders){
            orderCreateModels.add(transfromUtil.transformOrdToOCM(order));
        }
        return orderCreateModels;
    }
}

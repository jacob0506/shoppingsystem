package com.main.shoppingsystem.service;

import cn.hutool.core.util.IdUtil;
import org.springframework.stereotype.Service;

@Service
public class FlowNoFacade {

  public Long getNextId() {
    return IdUtil.getSnowflake(0,0).nextId();
  }
}
